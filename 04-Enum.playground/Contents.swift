// MARK: Задание 1:
// Создайте по 2 enum с разным типом RawValue.
enum Month: Int {
    case january   = 1
    case february  = 2
    case march     = 3
    case april     = 4
    case may       = 5
    case june      = 6
    case july      = 7
    case august    = 8
    case september = 9
    case october   = 10
    case november  = 11
    case december  = 12
}

enum Direction: String {
    case north = "N"
    case south = "S"
    case east  = "E"
    case west  = "W"
}

// MARK: Задание 2:
// Создайте несколько enum для заполнения полей стркутуры - анкета сотрудника: enum пол, enum категория возраста, enum стаж.
enum Gender {
    case male
    case female
}

enum AgeCategory {
    case young  // молодой возраст
    case middle // средний возраст
    case old    // пожилой возраст
}

enum Experience {
    case lessThanOneYear    // менее года
    case oneToFiveYears     // от 1 до 5 лет
    case fiveToTenYears     // от 5 до 10 лет
    case moreThanTenYears   // более 10 лет
}

struct EmployeeProfile {
    var name:        String
    var ageCategory: AgeCategory
    var gender:      Gender
    var experience:  Experience
}

let employee1 = EmployeeProfile(name: "Сергей", ageCategory: .young, gender: .male, experience: .fiveToTenYears)
let employee2 = EmployeeProfile(name: "Eгор", ageCategory: .young, gender: .male, experience: .oneToFiveYears)


// MARK: Задание 3:
// Создать enum со всеми цветами радуги.
enum RainbowColor {
    case red
    case orange
    case yellow
    case green
    case blue
    case indigo
    case violet
}


// MARK: Задание 4:
// Создать функцию, которая содержит массив разных case'ов enum и выводит содержимое в консоль. Пример результата в консоли 'apple green', 'sun red' и т.д.
enum FruitColor {
    case appleGreen
    case bananaYellow
    case grapePurple
    case lemonYellow
    case orangeOrange
    case peachPink
    case strawberryRed
}

func printFruitColors() {
    let fruitColors: [FruitColor] = [.appleGreen, .bananaYellow, .grapePurple, .lemonYellow, .orangeOrange, .peachPink, .strawberryRed]

    for color in fruitColors {
        switch color {
        case .appleGreen:
            print("Яблоко - зеленое")
        case .bananaYellow:
            print("Банан - желтый")
        case .grapePurple:
            print("Виноград - фиолетовый")
        case .lemonYellow:
            print("Лимон - желтый")
        case .orangeOrange:
            print("Апельсин - ораньжевый")
        case .peachPink:
            print("Персик - розовый")
        case .strawberryRed:
            print("Клубника - красная")
        }
    }
}

printFruitColors()


// MARK: Задание 5:
// Создать функцию, которая выставляет оценки ученикам в школе, на входе принимает значение enum Score: String {<Допишите case'ы} и выводит числовое значение оценки.
enum Score: String {
    case A = "Отлично"
    case B = "Хорошо"
    case C = "Удовлетворительно"
    case D = "Неудовлетворительно"
}

func gradeStudent(score: Score) -> Int? {
    switch score {
    case .A:
        return 5
    case .B:
        return 4
    case .C:
        return 3
    case .D:
        return 2
    default:
        return nil
    }
}

let studentScore = Score.A
if let grade = gradeStudent(score: studentScore) {
    print("Студент получил оценку \(grade)")
} else {
    print("Ошибка: недопустимая оценка")
}


// MARK: Задание 6:
// Создать метод, которая выводит в консоль какие автомобили стоят в гараже, используйте enum.
enum Car: String {
    case toyota   = "Toyota"
    case honda    = "Honda"
    case bmw      = "BMW"
    case mercedes = "Mercedes-Benz"
    case ford     = "Ford"
}

struct Garage {
    var cars: [Car]
    
    func listCars() {
        if cars.isEmpty {
            print("Гараж пуст.")
        } else {
            print("Автомобили в гараже:")
            for car in cars {
                print("- \(car.rawValue)")
            }
        }
    }
}

var myGarage = Garage(cars: [.bmw, .ford, .mercedes])
myGarage.listCars() 

var emptyGarage = Garage(cars: [])
emptyGarage.listCars()
